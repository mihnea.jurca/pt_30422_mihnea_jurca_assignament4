package com.utcn.bussiness.layer;

public class BaseProduct extends MenuItem{
	
	public BaseProduct(String baseProductName,int price) {
		
		this.menuItemName = baseProductName;
		
		this.price=price;
		
	}
	
	
	@Override
	public int computePrice() {
		
		return getPrice();
		
	}
	
	
	@Override
    public boolean equals(Object o) {
		
        if (this == o) {
        	
            return true;
            
        }
        
        if (o == null || getClass() != o.getClass()) {
        	
            return false;
            
        }
        
        BaseProduct simpson = (BaseProduct) o;
        
        return price == simpson.price && menuItemName.equals(simpson.menuItemName);
    }
}
