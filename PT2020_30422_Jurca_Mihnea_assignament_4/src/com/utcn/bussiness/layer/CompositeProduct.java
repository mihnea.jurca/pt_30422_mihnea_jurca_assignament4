package com.utcn.bussiness.layer;

import java.util.ArrayList;
import java.util.Iterator;



public class CompositeProduct extends MenuItem{
	
	private ArrayList<MenuItem> compositeProductList ;
	
	public CompositeProduct(ArrayList<MenuItem> compositeProductList) {
		
		this.compositeProductList=compositeProductList;
	}
	
	
	public int computePrice() {//total price
		
		int computeTotalPrice = 0;
		
		Iterator<MenuItem> iterator = compositeProductList.iterator();
		
		while(iterator.hasNext())//iterate throw the array list compute total price
		{
			
			MenuItem item = iterator.next();
			
			computeTotalPrice = computeTotalPrice + item.getPrice();
			
		}
		
		return computeTotalPrice;
	}

	@Override
	public String toString() {
		
		return "CompositeProduct [compositeProductList=" + compositeProductList + "]";
		
	}
	
	
}
