package com.utcn.bussiness.layer;

public abstract class MenuItem implements java.io.Serializable{
	
	protected String menuItemName;
	
	protected int price;
	
	
	
	public abstract int computePrice();

	
	
	public String getMenuItemName() {
		
		return menuItemName;
		
	}
	
	

	public void setMenuItemName(String menuItemName) {
		
		this.menuItemName = menuItemName;
		
	}
	
	

	public int getPrice() {
		
		return price;
		
	}
	
	

	public void setPrice(int price) {
		
		this.price = price;
		
	}



	@Override
	public String toString() {
		
		return "MenuItem [menuItemName=" + menuItemName + ", price=" + price + "]";
		
	}	
	
	
	
	
}
