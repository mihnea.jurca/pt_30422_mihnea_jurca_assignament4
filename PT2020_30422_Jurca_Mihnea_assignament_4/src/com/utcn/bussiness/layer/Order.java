package com.utcn.bussiness.layer;

import java.time.LocalDate;


public class Order {
	 
	private int orderID;
	private LocalDate date;
	private int table;
		
	
	
	public Order(int orderID, int table) {
		super();
		this.orderID = orderID;
		this.date = LocalDate.now();
		this.table = table;
	}

	
	
	public int hashCode(){
		
		final int prime = 31;
		
	    int result = 1;
	    
	    result = prime * result + (int) (table ^ (table >>> 32));
	    
	    result = prime * result + (int) (orderID ^ (orderID >>> 32));
	    
	    result = prime * result + ((date == null) ? 0 :date.hashCode());
	    
	    return result + 1;
	}
	
	public boolean equals(Object obj)
	{
		if(this==obj) return true;
		
		if(obj==null) return false;
		
		if(this.getClass() != obj.getClass()) return false;
		
		Order other = (Order) obj;
		
		if(this.date.equals(other.orderID)) return false;
		
		if(this.orderID!=other.orderID) return false;
		
		if(this.table != other.table) return false;
		
		return true;
		
	}



	public int getOrderID() {
		return orderID;
	}



	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}



	public LocalDate getDate() {
		return date;
	}



	public void setDate(LocalDate date) {
		this.date = date;
	}



	public int getTable() {
		return table;
	}



	public void setTable(int table) {
		this.table = table;
	}
	
	
	
}
