package com.utcn.bussiness.layer;

import java.util.ArrayList;

public interface IRestaurantProcessing {
	/*
	 * Admin 
	 * 	-Create menu item
	 * 	-delete menu item 
	 * 	-edit menu item
	 * 
	 * Waiter
	 * 	-new order
	 * 	-compute price for an order
	 * 	-generate bill in txt format
	 */
	
	/*
	 * @pre item not null
	 * @pre item !@exist
	 * @post list.size@pre == list.size@post + 1
	 */
	public boolean createMenuItem( MenuItem item );
	
	/*
	 * @pre item not null
	 * @pre item list@exist
	 * @post list.size@pre == list.size@post - 1
	 */
	public boolean deleteMenuItem( MenuItem item );
	
	/*
	 * @pre item not null
	 * @pre item @exist
	 * @post @forall k : [1 .. getSize() -1 ] @
	 * getElement(k)@pre == item
	 *@post list.size@pre == list.size@post 
	 */
	public void editMenuItem( MenuItem item );
	
	/*
	 * @pre order not null
	 * @pre order not @exist
	 * @post @nonchange
	 */
	public void createOrder(ArrayList<MenuItem> orderedItem, Order order);
	
	/*
	 * @pre item not null
	 * @pre item !@exist
	 * @post @forall k : [0 .. getSize() -1 ] 
	 * get list.computePrice
	 */
	public int priceOrder(Order order);
	
	/*
	 * 
	 */
	public void generateBill(Order order);
	
}
