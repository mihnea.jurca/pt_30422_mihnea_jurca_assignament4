package com.utcn.presentation.layer;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.utcn.bussiness.layer.Restaurant;

import javax.swing.JButton;
import java.awt.Color;

public class ChefGraphicalUserInterface extends JFrame implements Observer {

	private Restaurant restaurant;

	public ChefGraphicalUserInterface(Restaurant restaurant) {
		getContentPane().setBackground(Color.GRAY);
		setBackground(Color.ORANGE);

		this.restaurant = restaurant;

		restaurant.addObserver(this);

		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.setBounds(500, 150, 488, 491);
		this.getContentPane().setLayout(null);
		
		JLabel lblChef = new JLabel("Chef");
		lblChef.setFont(new Font("Lucida Bright", Font.PLAIN, 41));
		lblChef.setBounds(180, 69, 254, 80);
		getContentPane().add(lblChef);

	}

	@Override
	public void update(Observable o, Object arg) {
		this.setVisible(true);
		
		if (JOptionPane.showConfirmDialog(null, arg, "New order to cook", 2) == 0) {
			this.setVisible(false);
		} else {
			this.setVisible(false);
		}
	}
}

