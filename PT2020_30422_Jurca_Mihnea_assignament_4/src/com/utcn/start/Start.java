package com.utcn.start;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.crypto.spec.DESedeKeySpec;
import javax.jws.soap.SOAPBinding.Use;

import org.w3c.dom.UserDataHandler;

import com.utcn.bussiness.layer.BaseProduct;
import com.utcn.bussiness.layer.CompositeProduct;
import com.utcn.bussiness.layer.MenuItem;
import com.utcn.bussiness.layer.Restaurant;
import com.utcn.data.layer.RestaurantSerializator;
import com.utcn.presentation.layer.AdministratorGraphicalUserInterface;
import com.utcn.presentation.layer.ChefGraphicalUserInterface;
import com.utcn.presentation.layer.StartGui;
import com.utcn.presentation.layer.WaiterGraphicalUserInterface;



public class Start {

	//private WaiterGUI waiterGUI;
	
	
	
	private static StartGui s;

	public static void main(String args[])
	{
		PrintStream out;
		try {
			out = new PrintStream(new FileOutputStream("/Users/mihneajurk/Desktop/PT2020_30422_Jurca_Mihnea_Assignament4/PT2020_30422_Jurca_Mihnea_assignament_4/bill.txt"));
			System.setOut(out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Restaurant restaurant = new Restaurant();	
		MenuItem menuItem_1 = new BaseProduct("Supa", 18);
		MenuItem menuItem_2 = new BaseProduct("Cartofi", 10);
		MenuItem menuItem_3 = new BaseProduct("Carne", 20);
		MenuItem menuItem_4 = new BaseProduct("Suc", 10);
		
		ArrayList<MenuItem> compProduct = new ArrayList<MenuItem>();
		compProduct.add(menuItem_1);
		compProduct.add(menuItem_2);
		compProduct.add(menuItem_3);
		compProduct.add(menuItem_4);
		
		MenuItem menuItem_5 = new CompositeProduct(compProduct);
		menuItem_5.setMenuItemName("Meniul Zilei");
		menuItem_5.setPrice(menuItem_5.computePrice());
		
		restaurant.getMenuList().add(menuItem_1);
		restaurant.getMenuList().add(menuItem_2);
		restaurant.getMenuList().add(menuItem_3);
		restaurant.getMenuList().add(menuItem_4);
		restaurant.getMenuList().add(menuItem_5);
		
		s = new StartGui(restaurant);
		
	}
}
