package com.utcn.data.layer;


import java.io.*;

import com.utcn.bussiness.layer.Restaurant;


public class RestaurantSerializator {

	public static void serialize(Restaurant r) {
		try {
			FileOutputStream outPutFile = new FileOutputStream("Restaurant.ser");
			ObjectOutputStream out = new ObjectOutputStream(outPutFile);
			out.writeObject(r);
			out.close();
			outPutFile.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public static Restaurant deserialize() {
		Restaurant rest=null;
		try {
			FileInputStream inputFile = new FileInputStream("Restaurant.ser");
			ObjectInputStream in = new ObjectInputStream(inputFile);
			rest = (Restaurant) in.readObject();
			in.close();
			inputFile.close();
			return rest;
		} catch (IOException i) {
			 rest = new Restaurant();
			 serialize(rest);
			 return rest;
		} catch (ClassNotFoundException c) {

			c.printStackTrace();
			return rest = new Restaurant();
		}
	}
}
